import { useReducer } from "react";

let store = [];

export const combineReducers = (reducers = {}) => (state, actions) =>
  Object.keys(reducers).reduce(
    (acc, key) => ({ ...acc, [key]: reducers[key](acc[key], actions) }),
    state
  );

export function useInitStore(reducers, defaultStore) {
  const [state, dispatch] = useReducer(reducers, defaultStore);
  store = [state, dispatch];
}

export const useStore = () => store;
